import React from "react";
import ReactDOM from "react-dom";

export default class SearchForm extends React.Component {
    constructor(props) {
        super(props);
        this.displayImage = this.displayImage.bind(this);
    }

    displayResults() {

    }
    displayImage(url) {
        return <img src={url}/>;
    }
    render() {
        // console.log(this.props.result._embedded.self[0].fimg_url)
        let image = '';
        let result = this.props.result;
        if ( result._embedded.self[0].fimg_url ) {
            image = this.displayImage(result._embedded.self[0].fimg_url);
        }
        let results = '';

        if ( this.props.needMoreCharacters && !this.props.nothing) {
            results = <span>...</span>
        } else if ( this.props.loading && !this.props.nothing ) {
            results = <p>LOADING...</p>;
        } else if ( this.props.results && !this.props.loading ) {
            const searchResults = this.props.results.map( result => {
                return <SearchResult key={result.id} result={result}/>;
            } );
            results = <ul className="awcSearchResults block">{searchResults}</ul>;
        } else if ( this.props.nothing ) {
            results = <p>No results!!! ¯\_(ツ)_/¯</p>;
        }
        return (
            <li className="awcReactSearchResults">
                <a href={result.url}>
                    {image}
                    {result.title}
                </a>
            </li>
        );
    }
}
