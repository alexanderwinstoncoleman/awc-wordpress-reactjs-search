import React from "react";
import ReactDOM from "react-dom";
import SearchResult from './searchResult';

export default class SearchForm extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        let results = '';

        if ( this.props.needMoreCharacters && !this.props.nothing) {
            results = <span>...</span>
        } else if ( this.props.loading && !this.props.nothing ) {
            results = <p>LOADING...</p>;
        } else if ( this.props.results && !this.props.loading ) {
            const searchResults = this.props.results.map( result => {
                return <SearchResult key={result.id} result={result}/>;
            } );
            results = <ul className="awcSearchResults block">{searchResults}</ul>;
        } else if ( this.props.nothing ) {
            results = <p>No results!!! ¯\_(ツ)_/¯</p>;
        }
        return (
            <div className="awcReactSearchResults">
                {results}
            </div>
        );
    }
}
