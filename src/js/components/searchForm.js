import React from "react";
import SearchResults from "./searchResults";

export default class SearchForm extends React.Component {
    constructor(props) {
        super(props);
        this.noResults = this.noResults.bind(this);
        this.getResults = this.getResults.bind(this);
        this.showForm = this.showForm.bind(this);
        this.hideForm = this.hideForm.bind(this);
        this.searchForm = this.searchForm.bind(this);
        this.state = {
            loading: false, // nothing is happening
            searchData: [],
            nothing: false,
            needMoreCharacters: false,
            showForm: false,
        }
        this.searchResults = false;
    }
    getResults(e) {
        const search = e.target.value;
        if ( !search ) {
            this.setState({
                loading: false,
                searchData: [],
                nothing: true,
            });
            return;
        }

        if ( search && search.length < wp_react_js.minumum_characters ) {
            this.setState({
                searchData: [],
                needMoreCharacters: true,
                nothing: false
            });
        }

        if( search && search.length >= wp_react_js.minumum_characters ) {
            this.setState({
                needMoreCharacters: false,
                loading: true
            });
            let url = wp_react_js.rest_search_posts.replace("%s", search);
            let json = fetch(url)
            .then(response => {
            return response.json();
            })
            .then(results => {
                // console.log(results)
                if ( 0 < results.length ) {
                    this.setState({
                        searchData: results,
                        loading: false
                    });
                } else {
                    this.setState({nothing: true})
                }
            })
            .catch(error => console.log(error)); // We have results and are not loading anymore
        }
    }
    noResults() {
        const nothing = 'Sorry, nothing matches! ¯\\_(ツ)_/¯';
        this.searchResults = nothing;
        this.setState({searchData: nothing});
    }
    showForm() {
        this.setState({
            showForm: true,
        })
    }
    hideForm() {
        this.setState({
            showForm: false,
            loading: false,
            searchData: [],
        })
    }
    searchForm() {
        return <form className="awcFormElement">
        <label for="awcSearchInput" className="awcFormLabel">Search the Website</label>
        <input
            id="awcSearchInput"
            className="awcwp-search"
            placeholder="Search!!!!"
            onKeyUp={this.getResults}
            value={this.state.formValue}
        />
        <button className="button" aria-label="Search">Search</button>
    </form>
    }
    render() {
        let visible = '';
        if ( this.state.showForm ) {
            visible = ' show-form';
        }
        return (
            <div className="awcReactSearch awcSearchWrapper">
                <div className={"awcSearchForm" + visible} onMouseLeave={this.hideForm}>
                    {this.state.showForm === true && <this.searchForm/>}
                    <SearchResults
                        loading={this.state.loading}
                        results={this.state.searchData}
                        needMoreCharacters={this.state.needMoreCharacters}
                        nothing={this.state.nothing}
                        onMouseLeave={this.hideForm}
                    />
                </div>
                <i className="fal fa-search" aria-hidden="hidden" onMouseEnter={this.showForm} onTouchStart={this.showForm}></i>
            </div>
        );
    }
  }
