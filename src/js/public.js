import React from 'react';
import ReactDOM from 'react-dom';
import SearchForm from './components/searchForm';

const searchFormElement  = <SearchForm />;
const searchField = document.getElementById('awcSearch');


ReactDOM.render( searchFormElement, searchField );
