<?php
/**
 * Plugin Name: AWCWP React Search
 * Description: Replacing the search with React
 * Plugin URI: https://alexcoleman.io/reactjs-wordpress-search/
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

add_action( 'wp_enqueue_scripts', 'awc_react_search_rest_api_scripts' );

/**
* Enqueueing the script
*/
function awc_react_search_rest_api_scripts() {

    wp_enqueue_script( 'react-rest-js', plugin_dir_url( __FILE__ ) . 'assets/js/public.min.js', array(), '', true );
    wp_localize_script( 'react-rest-js', 'wp_react_js', array(
        // Adding the post search REST URL
        //   https://medium.com/@dalenguyen/how-to-get-featured-image-from-wordpress-rest-api-5e023b9896c6
        'rest_search_posts' => rest_url( 'wp/v2/search?search=%s&_embed' ),
        'minumum_characters' => 3,
        'rest_media_endpoint' => rest_url( 'wp/v2/media/' ),
        )
    );
    wp_script_add_data( 'react-rest-js', 'async', true );
    wp_script_add_data( 'react-rest-js', 'precache', true );
}

/**
* Add a custom link to the end of a specific menu that uses the wp_nav_menu() function
*/
// https://wpscholar.com/blog/append-items-to-wordpress-nav-menu/
add_filter('wp_nav_menu_items', 'add_admin_link', 10, 2);
function add_admin_link($items, $args){
    $items .= '<li id="awcSearch" class="awcSearch"></li>';
    return $items;
}

// ADDING FEATURED IMAGE TO RESULT
// https://medium.com/@dalenguyen/how-to-get-featured-image-from-wordpress-rest-api-5e023b9896c6

add_action('rest_api_init', 'register_rest_images' );

function register_rest_images(){
    register_rest_field( array('post', 'awc-portfolio'),
        'fimg_url',
        array(
            'get_callback'    => 'get_rest_featured_image',
            'update_callback' => null,
            'schema'          => null,
        )
    );
}

function get_rest_featured_image( $object, $field_name, $request ) {
    if( $object['featured_media'] ){
        // $img = wp_get_attachment_image_src( $object['featured_media'], 'app-thumb' );
        $img = wp_get_attachment_image_src( $object['featured_media'] );
        return $img[0];
    }
    return false;
}
