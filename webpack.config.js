const path = require('path');

module.exports = {
  entry: './src/js/public.js',
  output: {
    filename: 'public.min.js',
    path: path.resolve( __dirname, 'assets/js' )
  },
  optimization: {
    minimize: true
  },
  module: {
    rules: [
        {
            test: /\.js$/,
            exclude: /(node_modules|bower_components)/,
            use: {
                loader: 'babel-loader',
                options: {
                    presets: ['@babel/preset-env', '@babel/preset-react']
                }
            }
        }
    ]
  }
};
